# JavaScript Homeworks

## Úvod
V tomto repozitáři naleznete domácí úkoly a kódy z našich hodin JavaScript kurzu.

Úkoly jsou dobrovolné, ale silně doporučuji je průběžně vypracovávat. Úkol bude vždy zadán na konci hodiny. Já vaše práce vždy před následující hodinou projdu a napíšu vám komentář co zlepšit, upravit, čemu se vyvarovat nebo naopak co jste udělali dobře. Větší chyby by jsme si společně prošli na začátku další hodiny společně s mým vlastním řešením v rámci rychlého zopakování látky.

## Jak úkol odevzdat?

**Pokud už máte tento repozitář naklonovaný, ale nevidíte v něm složku s novým domácím úkolem, použijte příkaz ```git pull``` pro načtení změn ze vzdáleného repoziráře. Pokud jdete dělat váš první úkol, postupujte pomocí návodu níže.**

1. Naklonujete tento repozitář do vašeho vlastního počítače pomocí příkazu ```git clone https://gitlab.com/tom.jakuba/javascript-course-tomas-jakuba.git```
2. Přesunete do složky s konkrétním domácím úkolem (například složka s názvem hw01) pomocí příkazu ```cd ./javascript-course-tomas-jakuba``` a pak ```cd ./hw01```
3. Otevřete si složku ve Visual Studio Code pomocí příkazu ```code .```
4. V této složce naleznete soubor zadání.txt, který obsahuje zadání konkrétního úkolu
5. Vy si v této složce (např. hw01) vytvoříte vlastní složku se svým jménem pomocí příkazu ```mkdir tomasjakuba```, do které se přesunete pomocí příkazu ```cd ./tomasjakuba```
6. Ve vaší vlastní složce si vytvoříte soubory, které budete potřebovat (index.html, style.css,...) pomocí příkazu ```touch index.html``` a úkol zde vypracujete
7. Práci průběžně verzujte po větších blocích kódu (Např. poté co vytvoříte patičku, uděláte commit. Hned poté co uděláte sekci kontakt, uděláte commit. Atd.) pomocí ```git add filename``` a ```git commit -m "text commitu"```. První úkoly budou snadné, takže Vám bude stačit nejspíše jen jeden commit na celý úkol. Je ovšem důležité si zvyknout na průběžné verzování práce.
8. **Dokončenou práci je potřeba nahrát do vzdáleného repozitáře pomocí příkazu ```git push```**

```mermaid
graph TD;
  Homeworks-->HW01;
  Homeworks-->HW02;
  Homeworks-->HW03;
  Homeworks-->atd...;
  HW01-->zadani.txt;
  HW01-->tomasjakuba;
  HW01-->martinaoudova;
  HW01-->atd..;
  tomasjakuba-->index;
  tomasjakuba-->css;
  martinaoudova-->index_;
  martinaoudova-->css_;
```

Pokud budete mít problém s Gitem, hned mi napište. Mějte ovšem prosím strpení, tyto domácí úkoly dělám navíc ve svém volném času a nejsem dostupný 24/7. Úkol se snažte vypracovat samostatně - jsou dobrovolné.

### Materiály
Zde přikládám nějaké odkazy na rozšiřující materiály, které by vám mohli pomoci s vypracováním domácích úkolů nebo jako opakování. Najdete jistě mnoho dalších, ale tyto jsou podlě mě nejlepší. Průběžně je budu aktualizovat.

#### Git
- [Git](https://education.github.com/git-cheat-sheet-education.pdf)

#### CSS
- [CSS cheatsheet1](https://htmlcheatsheet.com/css/)
- [CSS cheatsheet2](https://www.geeksforgeeks.org/css-cheat-sheet-a-basic-guide-to-css/)
- [CSS kurz1](https://www.w3schools.com/css/)
- [CSS kurz2](https://web.dev/learn/css/)
- [CSS video crash course1](https://www.youtube.com/watch?v=1Rs2ND1ryYc)
- [CSS video crash course2](https://www.youtube.com/watch?v=yfoY53QXEnI&t=2s)

#### HTML
- [HTML cheatsheet1](https://htmlcheatsheet.com/)
- [HTML cheatsheet2](https://www.geeksforgeeks.org/html-cheat-sheet-a-basic-guide-to-html/)
- [HTML kurz1](https://www.w3schools.com/html/)
- [HTML video crash course1](https://www.youtube.com/watch?v=UB1O30fR-EE&t=1590s)
- [HTML video crash course2](https://www.youtube.com/watch?v=pQN-pnXPaVg)

#### Ostatní
- [Jak používat HTML keyboard shortcuts v .js souborech](https://stackoverflow.com/questions/64229807/how-to-use-keyboard-shortcuts-of-html-in-javascript-file-in-vs-code)

Obecně mohu doporučit YT kanál [Traversy Media](https://www.youtube.com/c/TraversyMedia), na kterém jsem se naučil spoustu věcí, které dnes umím.
